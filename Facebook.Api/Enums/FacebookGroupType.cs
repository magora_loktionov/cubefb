﻿namespace Facebook.Api.Enums
{
    public enum FacebookGroupType
    {
        Page,
        Group
    }
}
