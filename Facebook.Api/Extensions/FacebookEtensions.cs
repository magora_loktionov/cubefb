﻿using Facebook.Api.Models;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Facebook.Api.Extensions
{
    internal static class FacebookEtensions
    {
        internal async static Task<IEnumerable<TOut>> CollectAll<TIn, TOut>(this ResponseModel<TIn[]> items, Func<ResponseModel<TIn[]>, IEnumerable<TOut>> fn)
        {
            if (string.IsNullOrEmpty(items?.Paging?.Next))
            {
                return fn(items);
            }

            var result = await (await items.Paging.Next
                    .GetJsonAsync<ResponseModel<TIn[]>>())
                    .CollectAll(fn);

            return fn(items).Concat(result); 
        }
    }
}
