﻿using Facebook.Api.Extensions;
using Facebook.Api.Models;
using Flurl;
using Flurl.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Facebook.Api
{
    public partial class FacebookClient
    {
        protected const string GroupDefaultFields = "administrator,name,cover";
        protected const string AccountsDefaultFields = "can_post,picture,name,access_token";

        public async Task<FacebookGroupsAndAccountsModel> GetGroupsAndAccounts(string token)
        {

            var asdas = await GetGroups(token);

            var groupsAndAccounts = await Task.WhenAll(GetGroups(token), GetAccounts(token));
            return new FacebookGroupsAndAccountsModel
            {
                Accounts = (IEnumerable<FacebookAccountModel>)groupsAndAccounts.Last(),
                Groups = groupsAndAccounts.First()
            };
        }

        public async Task<IEnumerable<FacebookGroupModel>> GetGroups(string token) =>
            await (await "https://graph.facebook.com"
                .AppendPathSegments("me", "groups")
                .SetQueryParam("fields", GroupDefaultFields)
                .SetQueryParam("access_token", token)
                .GetJsonAsync<ResponseModel<GroupModel[]>>())
                .CollectAll(x => x.Data
                .Where(g => g.Administrator)
                .Select(g => new FacebookGroupModel
                {
                    Id = g.Id,
                    Name = g.Name,
                    Image = g.Cover?.Source,
                }));

        public async Task<IEnumerable<FacebookGroupModel>> GetAccounts(string token) =>
           await (await "https://graph.facebook.com"
                .AppendPathSegments("me", "accounts")
                .SetQueryParam("fields", AccountsDefaultFields)
                .SetQueryParam("access_token", token)
                .GetJsonAsync<ResponseModel<AccountModel[]>>())
                .CollectAll(x => x.Data
                .Where(p => p.CanPost)
                .Select(p => new FacebookAccountModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    Image = p.Picture?.Data?.Url,
                    Token = p.Token
                }));      
    }
}
