﻿using Facebook.Api.Models;
using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Facebook.Api
{
    public partial class FacebookClient
    {
        public async Task<dynamic> PublishToGroups(string token, FacebookPublishModel model)
        {
            try
            {
                var resultGroups = new List<string>();
                var resultAccounts = new List<string>();
                foreach (var group in model.Targets.Groups)
                {
                    var data = await PublishToGroup(token, group.Id, model.Message, model.Link, model.PublishTime);
                    resultGroups.Add(data);
                }

                foreach (var account in model.Targets.Accounts)
                {
                    var data = await PublishToAccount(account.Token, account.Id, model.Message, model.Link, model.PublishTime);
                    resultAccounts.Add(data);
                }
                return new
                {
                    resultAccounts, resultGroups
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Enumerable.Empty<string>();
            }

        }

        public async Task<string> PublishToGroup(string token, string groupId, string message, string link, long publishTime)
        {
            // TODO: Schedule publish.
            var response = await "https://graph.facebook.com"
                .AppendPathSegments(groupId, "feed")
                .SetQueryParam("access_token", token)
                .SetQueryParam("message", message)
                .SetQueryParam("link", link)
                .PostJsonAsync(null);

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> PublishToAccount(string token, string groupId, string message, string link, long publishTime)
        {
            var request = "https://graph.facebook.com"
                .AppendPathSegments(groupId, "feed")
                .SetQueryParam("access_token", token)
                .SetQueryParam("message", message)
                .SetQueryParam("link", link);

            if (publishTime > 0)
            {
                request = request.SetQueryParam("scheduled_publish_time", publishTime);
                request = request.SetQueryParam("published", false);
            }

            var response = await request.PostJsonAsync(null);

            return await response.Content.ReadAsStringAsync();
        }
    }
}
