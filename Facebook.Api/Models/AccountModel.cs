﻿using Newtonsoft.Json;

namespace Facebook.Api.Models
{
    internal class AccountModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("can_post")]
        public bool CanPost { get; set; }

        [JsonProperty("picture")]
        public ResponseModel<AccountPicture> Picture { get; set; }

        [JsonProperty("access_token")]
        public string Token { get; set; }
    }
}
