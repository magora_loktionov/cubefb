﻿using Newtonsoft.Json;

namespace Facebook.Api.Models
{
    public class ResponseModel<T>
    {
        [JsonProperty("data")]
        public T Data { get; set; }

        [JsonProperty("paging")]
        public ResponsePaging Paging { get; set; }


        public class ResponsePaging
        {
            [JsonProperty("cursors")]
            public ResponseCursors Cursors { get; set; }

            [JsonProperty("next")]
            public string Next { get; set; }
        }

        public class ResponseCursors
        {
            [JsonProperty("before")]
            public string Before { get; set; }

            [JsonProperty("after")]
            public string After { get; set; }
        }
    }
}
