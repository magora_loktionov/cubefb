﻿namespace Facebook.Api.Models
{
    public class FacebookAccountModel : FacebookGroupModel
    {
        public string Token { get; set; }
    }
}
