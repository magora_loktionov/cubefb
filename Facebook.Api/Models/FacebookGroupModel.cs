﻿namespace Facebook.Api.Models
{
    public class FacebookGroupModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }
}
