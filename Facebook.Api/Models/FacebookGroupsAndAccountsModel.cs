﻿using System.Collections.Generic;

namespace Facebook.Api.Models
{
    public class FacebookGroupsAndAccountsModel
    {
        public IEnumerable<FacebookAccountModel> Accounts { get; set; }
        public IEnumerable<FacebookGroupModel> Groups { get; set; }
    }
}
