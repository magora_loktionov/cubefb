﻿using Newtonsoft.Json;

namespace Facebook.Api.Models
{
    internal class GroupCover
    {
        [JsonProperty("cover_id")]
        public string CoverId { get; set; }

        [JsonProperty("offset_x")]
        public int OffsetX { get; set; }

        [JsonProperty("offset_y")]
        public int OffsetY { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}