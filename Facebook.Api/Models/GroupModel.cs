﻿using Newtonsoft.Json;

namespace Facebook.Api.Models
{
    internal class GroupModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("administrator")]
        public bool Administrator { get; set; }

        [JsonProperty("cover")]
        public GroupCover Cover { get; set; }
    }
}
