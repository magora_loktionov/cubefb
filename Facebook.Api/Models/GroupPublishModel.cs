﻿namespace Facebook.Api.Models
{
    public class FacebookPublishModel
    {
        public FacebookGroupsAndAccountsModel Targets { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
        public long PublishTime { get; set; }
    }
}
