﻿using Facebook.Api;
using Facebook.Api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace kube_online.Controllers
{
    public class HomeController : Controller
    {
        public async Task<IActionResult> Index([FromQuery] string token)
        {
            if (1 == 0)
                return await Test(token);

            return View();
        }

        public async Task<FacebookGroupsAndAccountsModel> Get([FromQuery] string token)
        {
            var client = new FacebookClient();
            return await client.GetGroupsAndAccounts(token);
        }

        [HttpPost]
        public async Task<dynamic> Publish([FromBody]FacebookPublishModel model, [FromQuery]string token)
        {
            var client = new FacebookClient();
            return await client.PublishToGroups(token, model);
        }

        public async Task<IActionResult> Test(string token)
        {
            // Set breakpoints and follow this lines to debug responses.

            /* Case 1: get a list of groups and accounts */

            var targets = await Get(token);


            /* Case 2: publish a message and a link to corresponding groups/accounts */
            var publishTime = DateTime.UtcNow.AddDays(2) - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var data = new FacebookPublishModel()
            {
                Targets = targets,
                Message = "привет как дела I hope u r well",
                Link = "https://cdn3.iconfinder.com/data/icons/social-icons-5/606/YouTube.png",
                PublishTime = (long)publishTime.TotalSeconds
            };
            var result = await Publish(data, token);

            return null;
        }
    }
}
